private array(array(string)) errors = ({ });
private array(array(string)) warnings = ({ });

void compile_error(string filename, int line, string msg)
{
    errors += ({ ({ filename, line, msg }) });
}

void compile_warning(string filename, int line, string msg)
{
    warnings += ({ ({ filename, line, msg }) });
}

string dump_errors()
{
    return sprintf("%{%s:%d:%s\n%}", errors);
}

array(array(string)) get_errors()
{
    return reverse(errors);
}

string dump_warnings()
{
    return sprintf("%{%s:%d:%s\n%}", warnings);
}

void purge()
{
    errors = ({ });
    warnings = ({ });
}
