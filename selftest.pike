import ".";

class TestCaseTest
{
    inherit TestCase;

    #define ASSERT_NO_ERROR(f, args...) \
        assert_selftest_no_error(f, __FILE__, __LINE__, args)
    #define ASSERT_HAS_ERROR(f, args...) \
        assert_selftest_has_error(f, __FILE__, __LINE__, args)

    int selftest_assert_true()
    {
        ASSERT_NO_ERROR(assert_true, true);
        ASSERT_HAS_ERROR(assert_true, false);

        ASSERT_NO_ERROR(assert_true, ({ }));
    }

    int selftest_assert_false()
    {
        ASSERT_NO_ERROR(assert_false, false);
        ASSERT_HAS_ERROR(assert_false, true);

        ASSERT_HAS_ERROR(assert_false, ({ }));
    }

    int selftest_assert_equal()
    {
        ASSERT_NO_ERROR(assert_equal, 1, 1);
        ASSERT_NO_ERROR(assert_equal, ({ }), ({ }));
        ASSERT_NO_ERROR(assert_equal, ([ ]), ([ ]));
        ASSERT_NO_ERROR(assert_equal, (< >), (< >));

        ASSERT_HAS_ERROR(assert_equal, 1, 2);
        ASSERT_HAS_ERROR(assert_equal, ({ }), ([ ]));

        // test assert_equal with arrays
        {
            array(string) arstring = ({ });
            array(int) arint = ({ });

            ASSERT_NO_ERROR(assert_equal, arstring, arint);

            arstring += ({ "test" });
            arint += ({ 123 });

            ASSERT_HAS_ERROR(assert_equal, arstring, arint);
        }

        // test assert_equal with mappings
        {
            mapping(string:int) mstringint = ([ ]);
            mapping(int:array) mintarray = ([ ]);

            ASSERT_NO_ERROR(assert_equal, mstringint, mintarray);

            mstringint["test1"] = 123;
            mintarray[123] = ({ "test1" });

            ASSERT_HAS_ERROR(assert_equal, mstringint, mintarray);
        }

        // test assert_equal with multisets
        {
            multiset(string) mstring = (< >);
            multiset(array) marray = (< >);

            ASSERT_NO_ERROR(assert_equal, mstring, marray);

            mstring["test1"] = 1;
            marray[({ 123 })] = 1;

            ASSERT_HAS_ERROR(assert_equal, mstring, marray);
        }
    }

    int selftest_assert_not_equal()
    {
        function f = assert_not_equal;

        ASSERT_NO_ERROR(f, 0, 1);
        ASSERT_NO_ERROR(f, 1, 2);
        ASSERT_NO_ERROR(f, 1, ({ }));
    }

    int do_selftests()
    {
        foreach (indices(this), string item)
        {
            if (has_prefix(item, "selftest_") && functionp(this[item]))
            {
                werror("Running %-68s", item);
                int ret;

                mixed e = catch {
                    ret = this[item]();
                };

                if(ret || e)
                {
                    werror("fail\n");
                    werror("%s\n", e);
                    return ret || 1;
                }

                werror("  ok\n");
            }
        }

        return 0;
    }

    private void
    assert_selftest_no_error(function f, string file, int line, mixed ... args)
    {
        mixed e = catch {
            f(file, line, @args);
        };

        if (e)
        {
            throw(sprintf("%s:%d: Failed to run %O(%O).\n%s\n", file, line, f,
                args, describe_backtrace(e)));
        }
    }

    private void
    assert_selftest_has_error(function f, string file, int line, mixed ... args)
    {
        mixed e = catch {
            f(file, line, @args);
        };

        if (!e)
        {
            throw(sprintf("Failed to run %O(%O).\n", f, args));
        }
    }
}

int main(int argc, array(string) argv)
{
    TestCaseTest tct = TestCaseTest();
    return tct->do_selftests();
}
