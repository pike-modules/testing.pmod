enum STATE {
    NOT_RUNNED = "not runned",
    OK = "ok",
    FAILED = "failed",
    CRASHED = "crashed",
    SKIPPED = "skipped",
};

STATE state = NOT_RUNNED;
string output;
string error;
string name;

string _sprintf()
{
    return sprintf("%s - %s\n", name, state);
}

void create(string n)
{
    name = n;
}
