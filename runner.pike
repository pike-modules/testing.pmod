#!/usr/bin/env pike

import ".";

private string frame(string name, string content)
{
        return sprintf(
                "%'='80s\n"
                "---%|' '74s---\n"
                "%'='80s\n"
                "%s\n"
                "%'='80s\n\n",
                "",
                name,
                "",
                content,
                "");
}

class TestGroup
{
    private string file;
    private object test_object;
    private program test_program;
    private Handler handler = Handler();
    private array(Test) tests;
    private mapping(string:int) results = ([ ]);
    private string errors = "";

    bool run_tests()
    {
        write("Running tests in %s ", file);
        foreach (tests, Test test)
        {
            mixed err = catch {
                test_object->run(test);
            };

            if (test->error)
            {
                errors += frame(test->name, test->error);
                write("F");
            }
            else
                write(".");
        }

        write("\n%s", errors);

        foreach (tests->state, string state)
            results[state] += 1;

        string summary = sprintf("\nTotal:%d ", sizeof(tests));
        foreach (results; string state; int value)
            summary += sprintf("%s:%d ", state, value);

        write(summary + "\n");
    }

    mapping(string:int) get_results()
    {
        return results;
    }

    string get_errors()
    {
        return errors;
    }

    void create(string fn)
    {
        file = fn;

        mixed e = catch {
           test_program = compile_string(Stdio.read_file(file), file, handler);
           test_object = test_program();
           foreach (test_object->get_test_items(), string testname)
                tests += ({ Test(testname) });
        };

        if (e)
            describe_status(e);
    }

    private void describe_status(mixed e)
    {
        write(frame(file, handler->dump_errors()));
    }
}

class Runner
{
    private array(TestGroup) groups;

    void create(array(TestGroup) g)
    {
        groups = g;
    }

    int run()
    {
        groups->run_tests();
        mapping(string:int) results = ([ ]);
        int num_tests = 0;
        foreach (groups->get_results(), mapping(string:int) result)
        {
            foreach (result; string state; int val)
            {
                results[state] += val;
                num_tests += val;
            }
        }

        string summary = sprintf("Total:%d ", num_tests);
        foreach (sort(indices(results)), string state)
            summary += sprintf("%s: %d ", state, results[state]);

        write("\n" + summary + "\n");

        if (results[Test.CRASHED] + results[Test.FAILED] > 0)
            return 1;

        return 0;
    }
}

int main(int argc, array argv)
{
    array(TestGroup) groups = ({ });
    foreach (argv; int index; string arg)
    {
        if (!index) continue;

        if (Stdio.is_file(arg) && arg[<3..] == "pike")
            groups += ({ TestGroup(arg) });
    }

    Runner runner = Runner(groups);
    return runner->run();
}
