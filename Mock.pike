import ".";

private string source;
private string filename;
private array(mapping(string:string)) mocks = ({ });
private object _target;
private string header = "";
private string footer = "";
private bool lazy_recompile = false;

object `()()
{
    return _target;
}

void create(string file, void|bool _lazy_recompile)
{
    if (!Stdio.exist(file))
        throw(sprintf("Source file %s doesn't exist.", file));

    source = Stdio.read_file(file);
    filename = file;

    lazy_recompile = _lazy_recompile;

    if (!lazy_recompile)
        _recompile(source);
}

void set_lazy_recompile(bool value)
{
    lazy_recompile = value;
}

void set_header(string h)
{
    header = h;
    _mock();
}

void set_footer(string f)
{
    footer = f;
    _mock();
}

private string escape_string(string pattern)
{
    array(string) from = ({
        "(",
        ")",
        "[",
        "]",
        "+",
        "*",
        "-",
        "?",
        "^",
        "$",
        "!"
    });
    array(string) to = ({ });
    foreach (from, string f)
        to += ({ "\\" + f });

    return replace(pattern, from, to);
}

private void _mock(void|bool force_recompile)
{
    string mocked_source = source;

    foreach (mocks, mapping(string:string) m)
    {
        string pattern = sprintf("%s[\t\n\r ]+%s",
            escape_string(m->return_type),
            escape_string(m->fname));

        if (stringp(m->modifiers) && strlen(m->modifiers))
        {
            pattern = sprintf("%s[\t\r\n ]+%s",
                    escape_string(m->modifiers), pattern);
        }

        string replacement = sprintf("%s\n%s %s mocked_%s",
            m->with, m->modifiers || "private", m->return_type, m->fname);

        mocked_source =
            Regexp.PCRE.Plain(pattern).replace(mocked_source, replacement);
    }

    if (!lazy_recompile || force_recompile)
    {
        _recompile(sprintf("%s\n#line 1\n%s\n%s\n",
                    header, mocked_source, footer));
    }
}

void mock(string fname, string return_type, string modifiers, string with)
{
    mocks += ({ ([
        "fname": fname,
        "return_type": return_type,
        "modifiers": modifiers,
        "with": with,
    ]) });

    _mock();
}

private void _recompile(string src)
{
    program prog;
    Handler handler = Handler();
    mixed e = catch {
        prog = compile(cpp(src, filename), handler);
        _target = prog();
    };

    if (e)
        throw(({ "MockFail:\n", handler->get_errors() }));
}

void recompile()
{
    _mock(true);
}
