import ".";

class AssertError(string fname, int line, string ... message)
{
    string _sprintf()
    {
        return sprintf("%s:%d: Assertion Error: ", fname, line)
                + sprintf(@message);
    }

    function `cast()
    {
        return _sprintf;
    }
}

protected void assert_true(string fname, int line, mixed a, void|string message)
{
    if (!!a)
        return;

    throw(AssertError(fname, line, "a is not true\na: %O\n%s", a, message || ""));
}

protected void assert_false(string fname, int line, mixed a, void|string message)
{
    if (!a)
        return;

    throw(AssertError(fname, line, "a is true\na: %O\n%s", a, message || ""));
}

protected void
assert_equal(string fname, int line, mixed a, mixed b, void|string message)
{
    if (multisetp(a) && multisetp(b) && multisets_equal(a, b))
        return;

    if (mappingp(a) && mappingp(b) && mappings_equal(fname, line, a, b))
        return;

    if (arrayp(a) && arrayp(b) && arrays_equal(fname, line, a, b))
        return;

    if (a == b)
        return;

    throw(AssertError(fname, line, "a != b\na: %O\nb: %O\n%s", a, b,
                message || ""));
}

protected void
assert_not_equal(string fname, int line, mixed a, mixed b, void|string message)
{
    if (multisetp(a) && multisetp(b) && !multisets_equal(a, b))
        return;

    if (mappingp(a) && mappingp(b) && !mappings_equal(fname, line, a, b))
        return;

    if (arrayp(a) && arrayp(b) && !arrays_equal(fname, line, a, b))
        return;

    if (a != b)
        return;

    throw(AssertError(fname, line, "a == b\na: %O\nb: %O\n%s", a, b,
                message || ""));
}

array(string) get_test_items()
{
    array(string) tests = ({ });
    foreach (indices(this); int index; string indice)
    {
        if (functionp(this[indice]) && has_prefix(indice, "test_"))
            tests += ({ indice });
    }

    return tests;
}

protected void tear_up() { ; }
protected void tear_down() { ; }

void run(Test test)
{
    mapping programs = copy_value(master()->programs);
    mixed e = catch {
        tear_up();
        this[test.name]();
        tear_down();
    };
    master()->programs = programs;

    if (e && object_program(e) == AssertError)
    {
        test.state = Test.FAILED;
        test.error = (string)e;
    }
    else if (e)
    {
        test.state = Test.CRASHED;
        test.error = describe_backtrace(e);
    }
    else
        test.state = Test.OK;
}

private bool mappings_equal(string fname, int line, mapping a, mapping b)
{
    if(sizeof(indices(a)) == sizeof(indices(b)))
    {
        foreach (a; mixed key; mixed value)
        {
            if (!has_index(b, key))
                return false;

            mixed e = catch {
                assert_equal(fname, line, value, b[key]);
            };

            if (e)
                return false;
        }

        return true;
    }

    return false;
}

private bool arrays_equal(string fname, int line, array a, array b)
{
    if(sizeof(a) == sizeof(b))
    {
        foreach (a; int index; mixed value)
        {
            mixed e = catch {
                assert_equal(fname, line, value, b[index]);
            };

            if (e)
                return false;
        }

        return true;
    }

    return false;
}

private bool multisets_equal(array a, array b)
{
    if(sizeof(indices(a)) == sizeof(indices(b)))
    {
        foreach (indices(a), mixed value)
        {
            if (!b[value])
                return false;
        }

        return true;
    }

    return false;
}
